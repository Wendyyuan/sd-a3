import itertools, random

class Card(object):
    def __init__(self,name,attack=0,money=0,cost=0,clan=None):
        self.name = name
        self.cost = cost
        self.attack = attack
        self.money = money
        self.clan = clan
    def __str__(self):
        return 'Name %s costing %s with attack %s and money %s' %(self.name, self.cost, self.attack, self.money)


class Player(object):
    def __init__(self,name,health=30,money=0,attack=0,deck=[],hand=[],active=[],handsize=5,discard=[]):
        self.name = name
        self.health = health
        self.money = money
        self.attack = attack
        self.deck = deck
        self.hand = hand
        self.active = active
        self.handsize = handsize
        self.discard = discard
    def get_health(self):
        print "%s Health %s" %(self.name,self.health)
    def get_values(self):
        print "%s Money %s, Attack %s" %(self.name,self.money,self.attack)
    def get_hand(self):
        print "\n Cards in %s Hand" %self.name
        for i in range(0,len(self.hand)):
            print "%d %s" %(i,self.hand[i])
    def get_active(self):
        print "\n Cards in %s Active Area" %self.name
        for i in range(0,len(self.active)):
            print "%d %s" %(i,self.active[i])
    def update_hand(self):
        for i in range(0,self.handsize):
            if(len(self.deck)==0):
                random.shuffle(self.discard)
                self.deck = self.disccard
                self.discard = []
            card = self.deck.pop()
            self.hand.append(card)


class Central(object):
    def __init__(self,name,deck=[],supplement=[],active=[],activesize=5):
        self.name = name
        self.deck = deck
        self.supplement = supplement
        self.active = active
        self.activesize = activesize
    def init_active(self):
        for i in range(0,activesize):
            card = self.deck.pop()
            self.active.append(card)
    def isempty_deck(self):
        if(len(self.deck)==0):
            return True
        else:
            return False
    def get_supplement(self):
        print "supplement Cards"
        if(len(self.supplement)>0):
            print self.supplement[0]
        else:
            print "no supplements left"
    def get_active(self):
        print "Available Cards for purchase"
        for i in range(0,len(self.active)):
            print "%d %s" %(i,self.active[i])
    def update_active(self):
        if (len(self.active) == 0):
            for i in range(0,self.activesize):
                if(len(self.deck)>0):
                    card = self.deck.pop()
                    self.active.append(card)
                else:
                    self.activesize = self.activesize - 1
        else:
            if(len(self.deck)>0):
                card = self.deck.pop()
                self.active.append(card)
            else:
                self.activesize = self.activesize - 1


def initi_game(player,computer,central):
    # builds all cards
    Archer = Card('Archer',3,0,2)
    Baker = Card('Baker',0,3,2)
    Swordsman = Card('Swordsman',4,0,3)
    Knight = Card('Knight',6,0,5)
    Tailor = Card('Tailor',0,4,3)
    Crossbowman = Card('Crossbowman',4,0,3)
    Merchant = Card('Merchant',0,5,4)
    Thug = Card('Thug',2,0,1)
    Thief = Card('Thief',1,1,1)
    Catapault = Card('Catapault',7,0,6)
    Caravan = Card('Caravan',1,5,5)
    Assassin = Card('Assassin',5,0,4)
    Serf = Card('Serf',0,1,0)
    Squire = Card('Squire',1,0,0)
    Levy = Card('Levy',1,2,2)
    
    # builds players
    deck = [8 * [Serf], 2 * [Squire]]
    player.deck = list(itertools.chain.from_iterable(deck))
    player.update_hand()
    computer.deck = list(itertools.chain.from_iterable(deck))
    computer.update_hand()
    
    # initialise central
    central_deck = [4 * [Archer], 4 * [Baker], 3 * [Swordsman], 2 * [Knight],
                    3 * [Tailor], 3 * [Crossbowman], 3 * [Merchant],
                    4 * [Thug], 4 * [Thief], 2 * [Catapault], 2 * [Caravan],
                    2 * [Assassin]]
    central.deck = list(itertools.chain.from_iterable(central_deck))
    random.shuffle(central.deck)
    central.supplement = 10 * [Levy]
    central.update_active()


def play(player):
    player.get_hand()
    mflag = True
    print "Do you want to play with all?"
    while mflag:
        choice1 = raw_input("Choose option,Y/N: ")
        if(choice1 == 'Y'):
            mflag = False
            playall(player)
        elif(choice1 == 'N'):
            Flag = True
            mflag = False
            while Flag:
                player.get_hand()
                player.get_active()
                print "Please choose the card to play[0-n], E to end choosing"
                choice2 = raw_input("Choose option, digital/E: ")
                if choice2 == 'E':
                    Flag = False
                elif(choice2.isdigit()):
                    if(len(player.hand)>0):
                        if(int(choice2)<len(player.hand)):
                            card = player.hand.pop(int(choice2))
                            player.active.append(card)
                            player.money = player.money + card.money
                            player.attack = player.attack + card.attack
                        else:
                            print "Please enter a valid index number"
                    else:
                        print "No hand cards left"
                        Flag = False
                else:
                    print "Please enter a valid option"
                player.get_values()
        else:
            print "Please enter a valid option"
            
    player.get_active()
    player.get_hand()

def playall(player):
    if(len(player.hand)>0):
        for i in range(0,len(player.hand)):
            card = player.hand.pop()
            player.active.append(card)
            player.money = player.money + card.money
            player.attack = player.attack + card.attack
    player.get_values()

    
def purchase(player,central):
    while player.money>0:
        player.get_values()
        print "Choose [1-n] to buy a card, choose S to buying the supplemental cards, choose E to end purchase"
        central.get_active()
        choice = raw_input("choose option: ")
        if choice == 'S':
            if (len(central.supplement)>0):
                if(player.money >= central.supplement[0].cost):
                    player.money = player.money - central.supplement[0].cost
                    card = central.supplement.pop()
                    player.discard.append(card)
                    print "Bought Successed"
                else:
                    print "Insufficient money to purchase the card"
            else:
                print "No supplements left"
        elif choice == 'E':
            break;
        elif choice.isdigit():
            if(int(choice)<len(central.active)):
                if(player.money >= central.active[int(choice)].cost):
                    player.money = player.money - central.active[int(choice)].cost
                    card = central.active.pop(int(choice))
                    player.discard.append(card)
                    print "Bought Successed"
                    central.update_active()
                else:
                    print "Insufficient money to purchase"
            else:
                print "Please enter a valid index number"
        else:
            print "Please enter a valid option"


# computer will purchase the most expensive card if the game is in Acquisitive pattern and purchase the most aggressive card if the game is in Aggressive pattern
def computer_purchase(player,central,choice):
        while player.money > 0:
            templist = []
            if(len(central.supplement)>0):
                if(player.money >= central.supplement[0].cost):
                    templist.append(("S",central.supplement[0]))
            for i in range(0,central.activesize):
                if(player.money >= central.active[i].cost):
                    templist.append((i,central.active[i]))
            if(len(templist)>0):
                maxiind = 0
                for i in range(0,len(templist)):
                    if(templist[i][1].cost>templist[maxiind][1].cost):
                        maxiind = i
                    elif(templist[i][1].cost == templist[maxiind][1].cost):
                        if(choice == 'A'):
                            if(templist[i][1].attack>templist[maxiind][1].attack):
                                maxiind = i
                        else:
                            if(templist[i][1].money>templist[maxiind][1].money):
                                maxiind = i
                source = templist[maxiind][0]
                if source in range(0,5):
                    player.money = player.money - central.active[source].cost
                    card = central.active.pop(source)
                    player.discard.append(card)
                    print "Card bought %s" %card
                    if(len(central.deck)>0):
                        card = central.deck.pop()
                        central.active.append(card)
                    else:
                        central.activesize = central.activesize - 1
                else:
                    player.money = player.money - central.supplement[0].cost
                    card = central.supplement.pop()
                    player.discard.append(card)
                    print "Supplement card bought %s" %card
            else:
                print "Insufficient money to purchase any card."
                break;
            if player.money == 0:
                print "No money to purchase cards"
                break;


# player1 is the attacker and player2 is attacked
def attack(player1,player2):
    player2.health = player2.health - player1.attack
    player1.attack = 0

def judge(player,computer,central,pG):
    if(player.health <= 0):
        print "Computer wins"
        pG = raw_input("Do you want to play another game? Y/N: ")
        if pG == 'Y':
            player = Player('player',30,0,0,[],[],[],5,[])
            computer = Player('computer',30,0,0,[],[],[],5,[])
            central = Central('central',[],[],[],5)
            initi_game(player,computer,central)
        else:
            exit()
    elif(computer.health <= 0):
        print "Player wins"
        pG = raw_input("Do you want to play another game? Y/N: ")
        if pG == 'Y':
            player = Player('player',30,0,0,[],[],[],5,[])
            computer = Player('computer',30,0,0,[],[],[],5,[])
            central = Central('central',[],[],[],5)
            initi_game(player,computer,central)
        else:
            exit()
    elif(central.isempty_deck()):
        print "The game ends because there is no more cards available."
        if(player.health > computer.health):
            print "Player wins on Health"
            pG = raw_input("Do you want to play another game? Y/N: ")
            if pG == 'Y':
                player = Player('player',30,0,0,[],[],[],5,[])
                computer = Player('computer',30,0,0,[],[],[],5,[])
                central = Central('central',[],[],[],5)
                initi_game(player,computer,central)
            else:
                exit()
        elif(play.health < computer.health):
            print "Computer wins on Health"
            pG = raw_input("Do you want to play another game? Y/N: ")
            if pG == 'Y':
                player = Player('player',30,0,0,[],[],[],5,[])
                computer = Player('computer',30,0,0,[],[],[],5,[])
                central = Central('central',[],[],[],5)
                initi_game(player,computer,central)
            else:
                exit()
        else:
            # calculate the sum of attack value of all cards in deck and discard
            if(len(player.deck)>0):
                for card in player.deck:
                    player.attack = player.attack + card.attack
            if(len(player.discard)>0):
                for card in player.discard:
                    player.attack = player.attack + card.attack
            if(len(computer.deck)>0):
                for card in computer.deck:
                    computer.attack = computer.attack + card.attack
            if(len(computer.discard)>0):
                for card in computer.discard:
                    computer.attack = computer.attack + card.attack
            if(player.attack > computer.attack):
                print "Player wins on card strength"
                pG = raw_input("Do you want to play another game? Y/N: ")
                if pG == 'Y':
                    player = Player('player',30,0,0,[],[],[],5,[])
                    computer = Player('computer',30,0,0,[],[],[],5,[])
                    central = Central('central',[],[],[],5)
                    initi_game(player,computer,central)
                else:
                    exit()
            elif(player.attack < computer.attack):
                print "Computer wins on card strength"
                pG = raw_input("Do you want to play another game? Y/N: ")
                if pG == 'Y':
                    player = Player('player',30,0,0,[],[],[],5,[])
                    computer = Player('computer',30,0,0,[],[],[],5,[])
                    central = Central('central',[],[],[],5)
                    initi_game(player,computer,central)
                else:
                    exit()
            else:
                print "Draw"
                pG = raw_input("Do you want to play another game? Y/N: ")
                if pG == 'Y':
                    player = Player('player',30,0,0,[],[],[],5,[])
                    computer = Player('computer',30,0,0,[],[],[],5,[])
                    central = Central('central',[],[],[],5)
                    initi_game(player,computer,central)
                else:
                    exit()

def end_tern(player):
    if(len(player.hand)>0):
        for i in range(0,len(player.hand)):
            card = player.hand.pop()
            player.discard.append(card)
    if(len(player.active)>0):
        for i in range(0,len(player.active)):
            card = player.active.pop()
            player.discard.append(card)
    for i in range(0,player.handsize):
        if(len(player.deck) == 0):
            random.shuffle(player.discard)
            player.deck = player.discard
            player.discard = []
        card = player.deck.pop()
        player.hand.append(card)
    player.money = 0
    player.attack = 0


if __name__ == '__main__':

    # initializes the game
    player = Player('player',30,0,0,[],[],[],5,[])
    computer = Player('computer',30,0,0,[],[],[],5,[])
    central = Central('central',[],[],[],5)
    initi_game(player,computer,central)
    conti = False
    pG = raw_input("Do you want to play the game? Y/N: ")
    if(pG == 'N'):
        exit()
    gP = raw_input("Do you want an aggressive pattern(A) or and acquisitive pattern(Q): A/Q: ")
    while pG == 'Y':
        flag = True
        print "\n It's player tern."

        while flag:
            
            computer.get_health()
            computer.get_values()

            player.get_health()
            player.get_values()

            player.get_hand()
            
            print "\n Choose Action: P = Play, B = Buy card, A = Attack, E = End your tern"
            act = raw_input("Enter action: ")
            if act == 'P':
                play(player)
            if act == 'B':
                purchase(player,central)
            if act == 'A':
                attack(player,computer)
            if act == 'E':
                end_tern(player)
                flag = False
            if act != 'P' and act != 'B' and act != 'A' and act != 'E':
                print "Please enter a valid action."

        # judges who is the winner
        judge(player,computer,central,pG)

        print "\n It's computer's turn"
        player.get_health()
        computer.get_health()
        player.get_values()
        computer.get_values()
        # computer always plays all cards
        playall(computer)
        # computer attacks player
        attack(computer,player)
        # computer always buy the most expensive card with highest attack in aggressive pattern or highest money in acquisative pattern
        computer_purchase(computer,central,gP)
        print "Computer'turn ends."
        end_tern(computer)
        # judges who is the winner
        judge(player,computer,central,pG)

        
            
                    
            
                    

    
